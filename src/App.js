import './App.css';
import AppNavbar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';

function App() {
  return (
    <>
      <AppNavbar/>
      <Home/>
      <Courses/>
    </>
  );
}

export default App;
