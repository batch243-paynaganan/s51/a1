import React, { useState } from 'react'
import { Row, Col, Button, Card } from 'react-bootstrap'

const CourseCard = ({coursesProp}) => {

  const  { id, name, description, price, slots } = coursesProp;
  // use the state hook for this component to be able to store its state specifically to monitor the number of employees
  // states are used to keep track information related to individual components
  // syntax:
    // const [getter, setter]=useState(initialGetterValue)

  const [enrollees, setEnrollees]=useState(0)
  const [slotsAvailable, setSlotsAvailable] = useState(slots)

  function enroll(){
    if(slotsAvailable>0){
      setEnrollees(enrollees+1)
      setSlotsAvailable(slotsAvailable-1)
    }else{
      document.getElementById(`btn${id}`).disabled = true;
      alert('no more seats.')
    }
  }
  return (
    <Row>
        <Col xs={12} md={4} className="offset-md-4 offset-0">
            <Card>
                <Card.Body>
                    <Card.Title as="h3">{name}</Card.Title>
                    <Card.Subtitle>Description</Card.Subtitle>
                    <Card.Text>
                    {description}
                    </Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>
                    PHP {price}
                    </Card.Text>
                    <Card.Subtitle>Enrollees:</Card.Subtitle>
                    <Card.Text>
                    {enrollees}
                    </Card.Text>
                    <Card.Subtitle>Slots available:</Card.Subtitle>
                    <Card.Text>
                    {slotsAvailable} slots
                    </Card.Text>
                    {slotsAvailable === 0 ? (
                        <>
                          <p style={{color: "red", fontStyle: "italic"}} >No more slots</p>
                          <Button id={`btn${id}`} variant="primary" onClick={enroll}>Enroll</Button>
                        </>
                          ) : (
                          <Button id={`btn${id}`} variant="primary" onClick={enroll}>Enroll</Button>
                          )}

                </Card.Body>
            </Card>
        </Col>
    </Row>
  )
}

export default CourseCard