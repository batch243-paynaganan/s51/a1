import React from 'react'
import { Row, Col,  Card } from 'react-bootstrap'

const Highlights = () => {
  return (
    <Row className='my-3'>
    {/* this is the first cart */}
        <Col xs={12} md={4}>
            <Card className='cardHighlight p-3'>
                <Card.Body>
                    <Card.Title><h2>Learn From Home</h2></Card.Title>
                    <Card.Text>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Placeat repudiandae corporis magni harum aperiam ducimus quo adipisci distinctio illum blanditiis debitis provident, quasi atque cupiditate vel cum iure sequi quos!
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    {/* this is my second cart */}
        <Col xs={12} md={4}>
            <Card className='cardHighlight p-3'>
                <Card.Body>
                    <Card.Title><h2>Study Now, Pay Later</h2></Card.Title>
                    <Card.Text>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Placeat repudiandae corporis magni harum aperiam ducimus quo adipisci distinctio illum blanditiis debitis provident, quasi atque cupiditate vel cum iure sequi quos!
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    {/* this is the third cart */}
        <Col xs={12} md={4}>
            <Card className='cardHighlight p-3'>
                <Card.Body>
                    <Card.Title><h2>Be Part of our Community</h2></Card.Title>
                    <Card.Text>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Placeat repudiandae corporis magni harum aperiam ducimus quo adipisci distinctio illum blanditiis debitis provident, quasi atque cupiditate vel cum iure sequi quos!
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    </Row>
  )
}

export default Highlights