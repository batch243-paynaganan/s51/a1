import React from 'react'
import { Button, Row, Col } from 'react-bootstrap'

const Banner = () => {
  return (
    <Row className='text-center'>
        <Col>
            <h1>Zuitt Coding Bootcamp</h1>
            <p>Opportunities for Everyone, Everywhere</p>
            
            <Button variant="primary">Enroll Now</Button>
        </Col>
    </Row>
  )
}

export default Banner