import React from 'react'
import CourseCard from '../components/CourseCard'
import coursesData from '../data/courses'


const Courses = () => {
   // console.log(coursesData)
   const courses = coursesData.map(course =>{
    return (
        <CourseCard key={course.id} coursesProp={course}/>
      )
   })
   return(
    <>
    {courses}
    </>
   )
}

export default Courses